const express = require('express')
const mongoose = require('mongoose');
const router =  express.Router()
const cors = require('cors')

router.use(express.json())
router.use(express.static('dist'))
router.use(cors())

mongoose.connect("mongodb://127.0.0.1:27017/try")
    .then(() => console.log('MongoDB Connected...'))
    .catch((err) => console.error(err));

const personSchema = new mongoose.Schema({
    name: String,
    number: String
});

const Person = mongoose.model('Person', personSchema);

router.get( "/api/persons" , (req, res)=>{
    Person.find({}).then(data=>{
        //console.log(data);
       res.json(data)
    })
})

module.exports = router;