const express = require('express')
const app = express()
const port = 3001

let persons = [
    { 
      "id": 1,
      "name": "Arto Hellas", 
      "number": "040-123456"
    },
    { 
      "id": 2,
      "name": "Ada Lovelace", 
      "number": "39-44-5323523"
    },
    { 
      "id": 3,
      "name": "Dan Abramov", 
      "number": "12-43-234345"
    },
    { 
      "id": 4,
      "name": "Mary Poppendieck", 
      "number": "39-23-6423122"
    },
    { 
        "id": 5,
        "name": "vd", 
        "number": "91-23-45634122"
      }
]

app.get('/api/persons/:id', (req,res) => {
    const id = Number(req.params.id);
    const per = persons.find(per => per.id === id)
    if (per) {
      res.json(per)
    } else {
      // res.status(404).end()
      res.send(`there is no data with the id ${req.params.id}`)
    }
})

app.listen(port, () => console.log(`Server running on port ${port}`))