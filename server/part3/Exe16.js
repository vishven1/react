const express = require('express')
const mongoose = require('mongoose');
const router =  express.Router()
const cors = require('cors')

router.use(express.json())
router.use(express.static('dist'))
router.use(cors())

mongoose.connect("mongodb://127.0.0.1:27017/try")
    .then(() => console.log('MongoDB Connected...'))
    .catch((err) => console.error(err));

const personSchema = new mongoose.Schema({
    name: String,
    number: String
});

personSchema.set('toJSON', {
    transform: (document, returnedObject) => {
      returnedObject.id = returnedObject._id.toString()
      delete returnedObject._id
      delete returnedObject.__v
    }
  })

router.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).json({ message: 'Internal Server Error' });
});  

const Person = mongoose.model('Person', personSchema);

router.get( "/api/persons" , (req, res)=>{
    Person.find({}).then(data=>{
       res.json(data)
    })
})

router.post("/api/persons",(req, res) => {
    const data = new Person ({
        name: req.body.name,
        number: req.body.number
    })
    data.save().then(()=>{
        res.json(data)})
})

router.delete("/api/persons/:id",(req,res) => {
    const id = req.params.id;
    Person.findByIdAndDelete(id)
    .then(result => {
      res.json(" Deleted succussfully ");
    })
})

module.exports = router;