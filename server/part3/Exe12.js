const express = require('express')
const mongoose = require('mongoose');
const router =  express.Router()

router.use(express.json())

mongoose.connect("mongodb://127.0.0.1:27017/try")
    .then(() => console.log('MongoDB Connected...'))
    .catch((err) => console.error(err));

const personSchema = new mongoose.Schema({
    name: String,
    number: String
});

const Person = mongoose.model('Person', personSchema);

router.post("/post",(req, res) => {

    const data = new Person ({
        name: req.body.name,
        number: req.body.number
    })
    data.save().then(()=>{
        res.json(data)})
})

module.exports = router;
// app.listen(port, () => console.log(`Server running on port ${port}`))