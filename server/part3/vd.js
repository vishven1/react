const express = require('express')
const app = express()
const mongoose = require('mongoose');
const port = process.env.port || 3001

app.use(express.json())

mongoose.connect("mongodb://localhost:27017/Phonebook").then(() => {
    console.log("conataing");
}).catch((err) => console.log("Mongo error", err))

//schema

const userSchema = new mongoose.Schema({
    name: String,
    number: String
})

const User = mongoose.model('user', userSchema)

app.get( "/get" , (req, res)=>{
    User.find({}).then(data=>{
        //console.log(data);
       res.json(data)
    })
})

app.post("/post",(req, res) => {

    const data = new User ({
        name: req.body.name,
        number: req.body.number
    })
    data.save().then(data=>{res.json(data)})
    // const val = data.save();
    // res.json(val)
    
})

app.listen(port, () => {
    console.log(`app listening on port ${port}`)
  })