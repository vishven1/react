const express = require('express')
const morgan = require('morgan') 
const router =  express.Router()
const cors = require('cors')


router.use(express.static('dist'))

morgan.token('postData', (req, res) => {
    if (req.method === 'POST') {
        return JSON.stringify(req.body);
    }
    return '';
});

router.use(cors())
router.use(express.json())
router.use(morgan(':method :url :status :res[content-length] - :response-time ms :postData'))

let persons = [
    { 
      "id": 1,
      "name": "Arto Hellas", 
      "number": "040-123456"
    },
    { 
      "id": 2,
      "name": "Ada Lovelace", 
      "number": "39-44-5323523"
    },
    { 
      "id": 3,
      "name": "Dan Abramov", 
      "number": "12-43-234345"
    },
    { 
      "id": 4,
      "name": "Mary Poppendieck", 
      "number": "39-23-6423122"
    },
    { 
        "id": 5,
        "name": "vd", 
        "number": "91-23-45634122"
      }
]

router.get('/api/persons/', (req,res) => {
    res.json(persons)
})

// router.get('/api/persons/:id', (req,res) => {
//     const id = Number(req.params.id);
//     const per = persons.find(per => per.id === id)
//     if (per) {
//       res.json(per)
//     } else {
//       res.send(`there is no data with the id ${req.params.id}`)
//     }
// })

router.post('/api/persons/', (req,res) => {
    const generateId = () => {
        const maxId = persons.length > 0
          ? Math.max(...persons.map(n => n.id))
          : 0
        return maxId + 1
      }
    const bName = req.body.name;
    if(bName==null || req.body.number==null){
        res.send(` please enter a valid name and number `)
    }else if(persons.some(n => n.name === bName)){
        res.send(` Enter a unique name `)
    }
    else{
        const per = {
            id: generateId(),
            name : req.body.name,
            number: req.body.number,
            
          } 
        persons = persons.concat(per)  
        res.json(per);
    }  
})

router.delete('/api/persons/:id', (req, res) => {
    const id = Number(req.params.id);
    persons = persons.filter(per => per.id !== id);
    res.json(persons);
})

module.exports = router;
// app.listen(port, () => console.log(`Server running on port ${port}`))