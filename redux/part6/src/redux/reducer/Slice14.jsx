import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  anecdotes: [],
  filter: 'all',
  status: 'idle',
  error: null
};

export const Slice14 = createSlice({
  name: 'anecdotes',
  initialState,
  reducers: {
    vote: (state, action) => {
      const { id } = action.payload;
      const anecdote = state.anecdotes.find(anec => anec.id === id);
      if (anecdote) {
        anecdote.votes++;
        state.anecdotes.sort((a, b) => b.votes - a.votes);
      }
    },
    addAnecdote: (state, action) => {
      state.anecdotes.push(action.payload);
    },
    setFilter: (state, action) => {
      state.filter = action.payload;
    },
    fetchAnecdotesStart: (state) => {
      state.status = 'loading';
      state.error = null;
    },
    fetchAnecdotesSuccess: (state, action) => {
      state.status = 'succeeded';
      state.anecdotes = action.payload;
    },
    fetchAnecdotesFailure: (state, action) => {
      state.status = 'failed';
      state.error = action.payload;
    }
  },
});

export const { vote, addAnecdote, setFilter, fetchAnecdotesStart, fetchAnecdotesSuccess, fetchAnecdotesFailure } = Slice14.actions;

export default Slice14.reducer;


