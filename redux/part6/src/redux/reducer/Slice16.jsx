import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

const initialState = {
  anecdotes: [],
  filter: 'all',
  status: 'idle',
  error: null
};

export const Slice16 = createSlice({
  name: 'anecdotes',
  initialState,
  reducers: {
    vote: (state, action) => {
      const { id } = action.payload;
      const anecdote = state.anecdotes.find(anec => anec.id === id);
      if (anecdote) {
        anecdote.votes++;
        state.anecdotes.sort((a, b) => b.votes - a.votes);
      }
    },
    addAnecdote: (state, action) => {
      state.anecdotes.push(action.payload);
    },
    setFilter: (state, action) => {
      state.filter = action.payload;
    },
    fetchAnecdotesStart: (state) => {
      state.status = 'loading';
      state.error = null;
    },
    fetchAnecdotesSuccess: (state, action) => {
      state.status = 'succeeded';
      state.anecdotes = action.payload;
    },
    fetchAnecdotesFailure: (state, action) => {
      state.status = 'failed';
      state.error = action.payload;
    }
  },
});


export const fetchAnecdotes = () => async (dispatch) => {
  dispatch(fetchAnecdotesStart());
  try {
    const response = await axios.get('http://localhost:3001/anecdotes');
    dispatch(fetchAnecdotesSuccess(response.data));
  } catch (error) {
    console.error('Error fetching anecdotes:', error);
    dispatch(fetchAnecdotesFailure(error.message));
  }
};

export const { vote, addAnecdote, setFilter, fetchAnecdotesStart, fetchAnecdotesSuccess, fetchAnecdotesFailure } = Slice16.actions;

export default Slice16.reducer;