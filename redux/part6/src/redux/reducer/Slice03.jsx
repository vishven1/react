import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  anecdote : [
  { id: 1, content: 'Writing code is like solving a puzzle. Each piece fits together to create a picture of functionality and beauty', votes: 0 },
  { id: 2, content: 'Debugging is like trying to find a needle in a haystack, except the needle is also made of hay, and the haystack is on fire', votes: 0 },
  { id: 3, content: 'I once tried to merge two branches without testing, and now they are both stuck in a never-ending conflict', votes: 0 },
  { id: 4, content: 'Programming is the closest thing we have to magic. You type a few words, and suddenly, the computer does exactly what you want (or what you told it to do)', votes: 0 },
  { id: 5, content: 'Programming is like writing poetry, but instead of words, you use symbols and syntax to weave stories in binary', votes: 0 },
  { id: 6, content: 'Writing code is like playing a game of chess against yourself. You make a move, anticipate your opponent next move (the compiler), and adjust your strategy accordingly.', votes: 0 },
  { id: 7, content: '"Programming is like a dance between logic and creativity, where the choreography is written in code and the stage is a computer screen', votes: 0 },
  { id: 8, content: 'Coding is like solving a mystery, except instead of clues, you have error messages, and instead of a detective hat, you have a keyboard', votes: 0 },
  { id: 9, content: 'I told my computer I needed a break, and it crashed. Guess it took my request too literally', votes: 0 },
  { id: 10, content: 'I tried to explain object-oriented programming to my cat, but she just stared at me like I was speaking in tongues. Then again, she thinks the mouse is a real mouse', votes: 0 },
],filter: 'all'}

export const Slice03 = createSlice({
    name: 'anecdotes',
    initialState,
    reducers: {
      vote: (state, action) => {
        const { id } = action.payload;
        const anecdoteToVote = state.anecdote.find(anecdote => anecdote.id === id);
        if (anecdoteToVote) {
          anecdoteToVote.votes++;
          state.anecdote.sort((a, b) => b.votes - a.votes);
        }
      },
      addAnecdote: (state, action) => {
        const { content } = action.payload;
        const id = state.anecdote.length + 1;
        state.anecdote.push({ id, content, votes: 0 });
        state.anecdote.sort((a, b) => b.votes - a.votes);
      },
      setFilter: (state, action) => {
        state.filter = action.payload;
      }
    }
})

// Action creators are generated for each case reducer function
export const { vote, addAnecdote, setFilter} = Slice03.actions

export default Slice03.reducer