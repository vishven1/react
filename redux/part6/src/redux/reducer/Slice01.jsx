import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    good: 0,
    ok: 0,
    bad: 0
}

export const Slice01 = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    incrementGood: (state) => {
      state.good += 1
    },
    incrementOk: (state) => {
      state.ok += 1
    },
    increamentBad: (state) => {
      state.bad += 1
    },
    reset: (state) =>{
        state.good =0
        state.ok =0
        state.bad =0
    }
  },
})

// Action creators are generated for each case reducer function
export const { incrementGood, incrementOk, increamentBad, reset} = Slice01.actions

export default Slice01.reducer