import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  cartItems: [],
  all : [{
    "id": 1,
    "title": "Blue T-shirt",
    "description": "Comfortable cotton t-shirt in blue color.",
    "price": 19.99,
    "image": "https://cdnp.sanmar.com/medias/sys_master/images/images/h01/h5d/8806071894046/4085-Black-5-2000LBlackFlatFront-1200W.jpg"
  },
  {
    "id": 2,
    "title": "Denim Jeans",
    "description": "Classic denim jeans for everyday wear.",
    "price": 39.99,
    "image": "https://example.com/denim-jeans.jpg"
  },
  {
    "id": 3,
    "title": "Black Hoodie",
    "description": "Warm and cozy black hoodie with front pocket.",
    "price": 29.99,
    "image": "https://gildan.my/wp-content/uploads/2020/02/63000-26C-Sapphire.png"
  },
  {
    "id": 4,
    "title": "Striped Dress",
    "description": "Stylish striped dress perfect for casual outings.",
    "price": 49.99,
    "image": "https://example.com/striped-dress.jpg"
  },
  {
    "id": 5,
    "title": "Running Shoes",
    "description": "Comfortable running shoes with cushioned sole.",
    "price": 59.99,
    "image": "https://example.com/running-shoes.jpg"
  },
  {
    "id": 6,
    "title": "Leather Jacket",
    "description": "Classic leather jacket for a stylish look.",
    "price": 99.99,
    "image": "https://example.com/leather-jacket.jpg"
  },
  {
    "id": 7,
    "title": "Printed Skirt",
    "description": "Colorful printed skirt for a chic ensemble.",
    "price": 39.99,
    "image": "https://example.com/printed-skirt.jpg"
  },
  {
    "id": 8,
    "title": "Cotton Sweater",
    "description": "Soft cotton sweater for warmth and comfort.",
    "price": 34.99,
    "image": "https://example.com/cotton-sweater.jpg"
  },
  {
    "id": 9,
    "title": "Formal Blazer",
    "description": "Tailored formal blazer for professional attire.",
    "price": 79.99,
    "image": "https://example.com/formal-blazer.jpg"
  },
  {
    "id": 10,
    "title": "Summer Dress",
    "description": "Lightweight summer dress for hot days.",
    "price": 29.99,
    "image": "https://example.com/summer-dress.jpg"
  }]

};

export const EcommerceSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    addToCart: (state, action) => {
      const { id } = action.payload;
      const productToAdd = state.all.find(product => product.id === id);
      if (productToAdd) {
        state.cartItems.push(productToAdd);
      }
    },
    price: (state, action) => {
      const total = state.cartItems.reduce((accumulator, currentValue) => {
        return accumulator + currentValue.price;
      }, 0);
      state.totalPrice = total;
    },
    removeFromCart: (state, action) => {
      let index = state.cartItems.indexOf(action.payload);
      if (index > -1) {
        state.cartItems.splice(index, 1);
      }
    },
  },
})

// Action creators are generated for each case reducer function
export const {addToCart,price,removeFromCart} = EcommerceSlice.actions

export default EcommerceSlice.reducer