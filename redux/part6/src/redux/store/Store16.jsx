import { configureStore } from '@reduxjs/toolkit'
import Slice16 from '../reducer/Slice16';

export const store = configureStore({
  reducer: {
    anecdotes: Slice16,
    },
})
