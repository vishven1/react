import { configureStore } from '@reduxjs/toolkit'
import Slice03 from '../reducer/Slice03';

export const store = configureStore({
  reducer: {
    anecdotes: Slice03,
    },
})
