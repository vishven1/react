import { configureStore } from '@reduxjs/toolkit'
import EcommerceSlice from '../reducer/EcommerceSlice';

export const store = configureStore({
  reducer: {
    data: EcommerceSlice,
    },
})
