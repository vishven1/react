import { configureStore } from '@reduxjs/toolkit'
import counterReducer from "../reducer/Slice01"

export const store = configureStore({
    reducer: {
        counter: counterReducer,
      },
})