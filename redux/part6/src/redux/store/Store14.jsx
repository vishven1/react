import { configureStore } from '@reduxjs/toolkit'
import Slice14 from '../reducer/Slice14';

export const store = configureStore({
  reducer: {
    anecdotes: Slice14,
    },
})
