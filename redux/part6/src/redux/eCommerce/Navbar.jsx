// Exercise 6.1 and 6.2
import { useSelector, useDispatch } from 'react-redux'
import { incrementGood, increamentBad, incrementOk, reset} from '../reducer/Slice01'
import { Link } from 'react-router-dom'

function Navbar() {
  return (
    <>
    <div>
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <Link className="navbar-brand" to="#">Navbar</Link>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div className="navbar-nav">
      <Link className="nav-item nav-link active" to="/">Home <span className="sr-only">(current)</span></Link>
      <Link className="nav-item nav-link" to="/wishlist"> Wishlist </Link>
      <Link className="nav-item nav-link" to="/price">Pricing</Link>
    </div>
  </div>
</nav>
      

    </div> 
    </>
  )
}

export default Navbar
