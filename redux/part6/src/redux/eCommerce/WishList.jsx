import { useSelector, useDispatch } from 'react-redux'
import React from 'react'
import { Link } from 'react-router-dom'
import { removeFromCart } from '../reducer/EcommerceSlice';

export default function WishList() {

    const products = useSelector(state => state.data.cartItems);
    const dispatch = useDispatch()

  return (
    <div className="container">
          <div className="row">
            {products.map(product => (
              <div key={product.id} className="col-12 col-md-6 col-lg-3 mb-3">
                <div className="card">
                  <img src={product.image} className="card-img-top" alt={product.title} />
                  <div className="card-body">
                    <h5 className="card-title">{product.title}</h5>
                    <p className="card-text">{product.description}</p>
                    <p className="card-text">${product.price}</p>
                    <button className="btn btn-primary" onClick={() => handleAddToCart(product)}>Add to Cart</button>
                  </div>
                </div>
              </div>
            ))}
          </div>
          
          <div>
          <center> 
          <Link to="/price">
          <button className="btn btn-success"> BUY NOW </button>
          </Link> 
          </center>  
          </div>
        </div>
  )
}
