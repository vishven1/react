// Exercise 6.1 and 6.2
import { useSelector, useDispatch } from 'react-redux'
import { addToCart } from '../reducer/EcommerceSlice';

function Home() {

const products = useSelector(state => state.data.all);
const handleAddToCart = (product) => {
  dispatch(addToCart(product));
};
const dispatch = useDispatch()

  return (
  <>
          {/* <div className="card-group" style={{width: '18rem' }}>
          <div className="card" style={{width: '18rem' }}>
            {products.map(product => (
              <div className="card" key={product.id}>
                <img src={product.image} className="card-img-top" alt={product.title} />
                <div className="card-body" >
                  <h5 className="card-title">{product.title}</h5>
                  <p className="card-text">{product.description}</p>
                  <p className="card-text">${product.price}</p>
                  <button className="btn btn-primary" onClick={() => handleAddToCart(product)}>Add to Cart</button>
                </div>
              </div>
            ))}
          </div>
          </div> */}




    <div className="container">
          <div className="row">
            {products.map(product => (
              <div key={product.id} className="col-12 col-md-6 col-lg-3 mb-3">
                <div className="card">
                  <img src={product.image} className="card-img-top" alt={product.title} />
                  <div className="card-body">
                    <h5 className="card-title">{product.title}</h5>
                    <p className="card-text">{product.description}</p>
                    <p className="card-text">${product.price}</p>
                    <button className="btn btn-primary" onClick={() => handleAddToCart(product)}>Add to Cart</button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
</>
)
}

export default Home
