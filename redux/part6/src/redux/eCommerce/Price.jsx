import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

export default function Price() {

    const cartItems = useSelector(state => state.data.cartItems);
    const totalPrice = cartItems.reduce((total, item) => total + item.price, 0);

    // const product = useSelector(state => state.data.cartItems);
    // const dispatch = useDispatch()

    // const handlePrice = (product) => {
    //     dispatch(price(product));
    //   };

  return (

    <div>
      <center>  
      <h2> Your Bill </h2> <br />
      {/* Display cart items */}
      {cartItems.map(item => (
        <div key={item.id}>
          <p>{item.title} - ${item.price}</p>
        </div>
      ))}
      {/* Display total price */}
      <p>Total Price: ${totalPrice}</p>

      <button className="btn btn-success"> Pay ${totalPrice} </button>
      </center>

      
    </div>
  )
}
