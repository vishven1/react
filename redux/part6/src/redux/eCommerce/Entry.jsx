import React from 'react'
import Navbar from './Navbar'
import Home from './Home'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import WishList from './WishList';
import Price from './Price';

export default function Entry() {
  return (
    <div>
        
      <Router>
      <Navbar/>
        <Routes>
          <Route path="/" element={<Home/>} />  
          <Route path="/wishlist" element={<WishList/>} /> 
          <Route path="/price*" element={<Price/>} />
        </Routes>  
      </Router>
    </div>
  )
}
