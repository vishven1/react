import React from 'react';
import { useQuery, useMutation, useQueryClient } from 'react-query';
import { useDispatch } from 'react-redux';
import { vote } from '../reducer/Slice16';

const fetchAnecdotes = async () => {
  const response = await fetch('http://localhost:3001/anecdotes');
  if (!response.ok) {
    throw new Error('Failed to fetch anecdotes');
  }
  return response.json();
};

const addAnecdote = async (content) => {
  const response = await fetch('http://localhost:3001/anecdotes', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ content }),
  });
  if (!response.ok) {
    throw new Error('Failed to add anecdote');
  }
  return response.json();
};

function Exercise20() {
  const dispatch = useDispatch();
  const queryClient = useQueryClient();

  const { data: anecdotes, isLoading, isError, error } = useQuery('anecdotes', fetchAnecdotes);

  const mutateAddAnecdote = useMutation(addAnecdote, {
    onSuccess: (data) => {
      queryClient.setQueryData('anecdotes', (prevAnecdotes) => [...prevAnecdotes, data]);
    },
  });

  const handleAddAnecdote = async () => {
    const content = prompt('Enter your anecdote:');
    if (content && content.length >= 5) {
      mutateAddAnecdote.mutate(content);
    } else {
      alert('Anecdote content must be at least 5 characters long');
    }
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (isError) {
    return <div>Error: {error.message}</div>;
  }

  const handleVote = id => {
    dispatch(vote({ id }));
  };

  return (
    <>
      <h1>Anecdotes</h1>
      <button onClick={handleAddAnecdote}>Add Anecdote</button>
      <ul>
        {anecdotes.map((anecdote) => (
          <li key={anecdote.id}>
            <div>{anecdote.content}</div>
            <div>Votes: {anecdote.votes}</div>
            <button onClick={() => handleVote(anecdote.id)}>Vote</button>
          </li>
        ))}
      </ul>
    </>
  );
}

export default Exercise20;
