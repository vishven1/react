import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment} from "../reducer/counterSlice"

function Exercise01() {
// const counterReducer = (state = 0, action) => {
//     switch (action.type) {
//       case 'INCREMENT':
//         return state + 1
//       case 'DECREMENT':
//         return state - 1
//       case 'ZERO':
//         return 0
//       default: 
//         return state
//     }
// }
// const store = createStore(counterReducer)
// console.log(store.getState())
// store.dispatch({ type: 'INCREMENT' })
// store.dispatch({ type: 'INCREMENT' })
// store.dispatch({ type: 'INCREMENT' })
// console.log(store.getState())
// store.dispatch({ type: 'ZERO' })
// store.dispatch({ type: 'DECREMENT' })
// console.log(store.getState())
// store.dispatch(store)

const count = useSelector((state) => state.counter.value)
const dispatch = useDispatch()

  return (
    <>
    <div>
      <button onClick={() => dispatch(decrement())}> - </button>
     currently counter is 
      
      {count}
      <button onClick={() => dispatch(increment())}> + </button>
    </div> 
    </>
  )
}

export default Exercise01
