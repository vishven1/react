// Exercise 6.1 and 6.2
import { useSelector, useDispatch } from 'react-redux'
import { incrementGood, increamentBad, incrementOk, reset} from '../reducer/Slice01'

function Exercise01() {

const goodCount = useSelector(state => state.counter.good);
const okCount = useSelector(state => state.counter.ok)  
const badCount = useSelector(state=> state.counter.bad);
//const count = useSelector((state) => state.slice.good)
const dispatch = useDispatch()

  return (
    <>
    <div>

      <button onClick={() => dispatch(incrementGood())}> Good </button>
      <button onClick={() => dispatch(incrementOk())}> Average </button>
      <button onClick={() => dispatch(increamentBad())}> Bad </button>
      <button onClick={() => dispatch(reset())}> Reset </button> <br />
      Good : {goodCount} <br />
      Average : {okCount} <br />
      Bad : {badCount} <br />

    </div> 
    </>
  )
}

export default Exercise01
