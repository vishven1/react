import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { vote, fetchAnecdotesStart, fetchAnecdotesSuccess, fetchAnecdotesFailure } from '../reducer/Slice14';
import axios from 'axios';

function Exercise14() {
  const [searchTerm, setSearchTerm] = useState('');
  const [notification, setNotification] = useState('');
  const [content, setContent] = useState('');
  const anecdotes = useSelector(state => state.anecdotes.anecdotes);
  const dispatch = useDispatch();

  useEffect(() => {
    fetchAnecdotes();
  }, []);

  const fetchAnecdotes = async () => {
    dispatch(fetchAnecdotesStart());
    try {
      const response = await axios.get('http://localhost:3001/anecdotes');
      dispatch(fetchAnecdotesSuccess(response.data));
    } catch (error) {
      console.error('Error fetching anecdotes:', error);
      dispatch(fetchAnecdotesFailure(error.message));
    }
  };

  const style = {
    padding: 10,
    borderWidth: 1,
  };

  const handleVote = id => {
    dispatch(vote({ id }));
  };

  const handleSubmit = async event => {
    event.preventDefault();
    try {
      await axios.post('http://localhost:3001/anecdotes', {
        content,
        votes: 0
      });
      setNotification(`New Anecdote "${content}" Added`);
      setTimeout(() => {
        setNotification(null);
      }, 3000);
      setContent('');
      fetchAnecdotes();
    } catch (error) {
      console.error('Error adding anecdote:', error);
    }
  };

  const handleSearch = event => {
    setSearchTerm(event.target.value);
  };

  const filteredAnecdotes = anecdotes.filter(anecdote =>
    anecdote.content.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <>
      <div>
        <h2> Anecdotes </h2>
        <div style={style}> <center> {notification} </center></div>
        <input
          type="text"
          placeholder="Search anecdotes"
          value={searchTerm}
          onChange={handleSearch}
        />
        {filteredAnecdotes.map(anecdote =>
          <div key={anecdote.id}>
            <div>
              {anecdote.content} <br />
              Votes: {anecdote.votes} <br />
              <button onClick={() => handleVote(anecdote.id)}>Vote</button>
            </div>
          </div>
        )}
        <br />
        <div>
          <form onSubmit={handleSubmit}>
            <input
              type="text" placeholder="Enter your anecdote" value={content} onChange={(event) => setContent(event.target.value)} />
            <button type="submit">Add Anecdote</button>
          </form>
        </div>
      </div>
    </>
  );
}

export default Exercise14;
