//Exercise 6.3-6.13

import { useSelector, useDispatch } from 'react-redux'
import { vote, addAnecdote, setFilter} from '../reducer/Slice03'
import { useEffect, useState } from 'react';

function Exercise03() {

const anec = useSelector(state => state.anecdotes.anecdote);
const [searchTerm, setSearchTerm] = useState('');
const [notification, setNotification] = useState();
const dispatch = useDispatch()
const [content, setContent] = useState('');

const style = {
  padding: 10,
  borderWidth: 1,
}

const handleVote = id => {
  dispatch(vote({ id }));
};

const handleSubmit = (event) => {
  event.preventDefault();
  dispatch(addAnecdote({ content }));
  setNotification( `New Anecdote ${content} Added`)
  setTimeout(() => {
    setNotification(null);
  }, 3000);
  setContent('');
};

const handleSearch = event => {
  setSearchTerm(event.target.value);
};

const filteredAnecdotes = anec.filter(anec =>
  anec.content.toLowerCase().includes(searchTerm.toLowerCase())
);

  return (
    <>
    <div>
        <h2> Anecdotes </h2>
        <div style={style}> <center> {notification} </center></div>
        <input
          type="text"
          placeholder="Search anecdotes"
          value={searchTerm}
          onChange={handleSearch}
        />
        {filteredAnecdotes.map(anec=>
          <div key={anec.id}>
            <div>
              {anec.content} <br />
              Votes: {anec.votes} <br />   
              <button onClick={() => handleVote(anec.id)}>Vote</button>
            </div>
          </div>
         )} <br />
         <div>
         <form onSubmit={handleSubmit}>
      <input
        type="text" placeholder="Enter your anecdote" value={content} onChange={(event) => setContent(event.target.value)}/>
      <button type="submit">Add Anecdote</button>
    </form>
         </div>
    </div> 
    </>
  )
}

export default Exercise03
