import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { fetchAnecdotes, vote } from '../reducer/Slice16';

function Exercise16() {
  const dispatch = useDispatch();
  const anecdotes = useSelector(state => state.anecdotes.anecdotes);
  const status = useSelector(state => state.anecdotes.status);
  const error = useSelector(state => state.anecdotes.error);

  useEffect(() => {
    dispatch(fetchAnecdotes());
  }, [dispatch]);

  if (status === 'loading') {
    return <div>Loading...</div>;
  }

  if (status === 'failed') {
    return <div>Error: {error}</div>;
  }

  const handleVote = id => {
    dispatch(vote({ id }));
  };

  return (
    <>
      <h1>Anecdotes</h1>
      <ul>
        {anecdotes.map(anecdote => (
          <li key={anecdote.id}>
            <div>{anecdote.content}</div>
            <div>Votes: {anecdote.votes}</div>
            <button onClick={() => handleVote(anecdote.id)}>Vote</button>
          </li>
        ))}
      </ul>
    </>
  );
}

export default Exercise16;