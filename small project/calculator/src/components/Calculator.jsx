import React, { useState } from 'react'
export default function Calculator() {   
    const [result, setResult] = useState('');
    const [input, setInput] = useState('');
    const reset = () => {
        setInput('');
       // setResult('');
    }
    const update = (prop) => {
        setInput(input + prop);
    }
    const equal = () => {
        const result = eval(input);
        setResult(result);
        setInput(result.toString());
        localStorage.setItem(input, result);
    }
    let archive = [];
    const listItems = archive.map(person =>
        <h1>{person}</h1>
      );
    // const seeHistory=()=>{
        
    //     for (let i = 0; i<localStorage.length; i++) {
    //         archive[i] = localStorage.getItem(localStorage.key(i));
    //         console.log(archive[i])
    //     } 
    // }

    return (
    <div>
        <h1> CALCULATOR </h1> <br />
            <input type="text" value={input} onChange={(e) => setInput(e.target.value)} style={{ fontSize: '22px' }} /> <br/>
            <button onClick={reset}> AC </button>
            <button> +/- </button>
            <button onClick={() => update("%")}> % </button>
            <button onClick={() => update("/")}> div </button> <br />
            <button onClick={() => update("7")}> 7 </button>
            <button onClick={() => update("8")}> 8 </button>
            <button onClick={() => update("9")}> 9 </button>
            <button onClick={() => update("*")}> X </button> <br />
            <button onClick={() => update("4")}> 4 </button>
            <button onClick={() => update("5")}> 5 </button>
            <button onClick={() => update("6")}> 6 </button>
            <button onClick={() => update("-")}> - </button> <br />
            <button onClick={() => update("1")}> 1 </button>
            <button onClick={() => update("2")}> 2 </button>
            <button onClick={() => update("3")}> 3 </button>
            <button onClick={() => update("+")}> + </button> <br />
            <button onClick={() => update("0")}> 0 </button>
            <button onClick={() => update(".")}> . </button>
            <button onClick={equal}> = </button>
            {/* <button onClick={seeHistory}> His </button> <br /> */}
            <br /> <br />
            {result!=input ? `YOUR previous ANSWER was `: `YOUR TOTAL ANSWER IS `}
            <h1>{result}</h1>
            <p>     {archive}</p> 
       </div>
    )
}
