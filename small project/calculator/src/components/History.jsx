import React, { useEffect, useState } from 'react';


export default function History() {
  const [archive, setArchive] = useState([]);

  const getAllStorage = () => {
    const newArchive = [];
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);
      const value = localStorage.getItem(key);
      newArchive.push(`operation = ${key} : result = ${value}`);
    }
    setArchive(newArchive);
  }

  useEffect(() => {
    getAllStorage();
  }, []);

  return (
    <>

<div>
    <br /><br />
      <h2> Your Previous Calculations </h2>
      {archive.map((item, index) => (
        <p key={index}>{item}</p>
      ))}
    </div>
    </>
  );
}
