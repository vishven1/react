import React, { useState } from 'react';

function Calc() {
  const [input, setInput] = useState('');
  const [result, setResult] = useState('');

  const update = (number) => {
    setInput(input + number);
  };

  const handleEqualClick = () => {
    const result = eval(input);
    setResult(result);
    setInput(result.toString());
    localStorage.setItem(input, result);
  };

  return (
    <div>
      <input type="text" value={input} /> <br/>
            <button> +/- </button>
            <button> % </button>
            <button> div </button> <br />
            <button onClick={() => update("7")}> 7 </button>
            <button onClick={() => update("8")}> 8 </button>
            <button onClick={() => update("9")}> 9 </button>
            <button onClick={() => update("X")}> X </button> <br />
            <button onClick={() => update("4")}> 4 </button>
            <button onClick={() => update("5")}> 5 </button>
            <button onClick={() => update("6")}> 6 </button>
            <button onClick={() => update("-")}> - </button> <br />
            <button onClick={() => update("1")}> 1 </button>
            <button onClick={() => update("2")}> 2 </button>
            <button onClick={() => update("3")}> 3 </button>
            <button onClick={() => update("+")}> + </button> <br />
            <button onClick={() => update("0")}> 0 </button>
            <button onClick={handleEqualClick}>=</button>
      <p>{result}</p>
    </div>
  );
}

export default Calc;