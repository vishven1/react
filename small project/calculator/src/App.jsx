import { useState } from 'react'
import './App.css'
import Calculator from './components/Calculator'
import Calc from './components/Cal'
import History from './components/History'
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom' 
import Navbar from './components/Navbar'



function App(props) {

  return (
    <>
    {/* <History/>
    <Calculator/> */}
    
      {/* <Calc/> */}
      <Router>
        <Navbar/>
      <Routes>
        <Route path="/history" element={<History/>}/>
        <Route path="/" element={<Calculator/> } />
      </Routes>
      </Router>
    </> 
  )
}

export default App
