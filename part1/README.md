Name :-

FullStackOpen Course

Description :-

Learning React, Redux, Node.js, MongoDB, GraphQL and TypeScript in one go! This course will introduce to modern JavaScript-based web development. The main focus is on building single page applications with ReactJS that use REST APIs built with Node.js.

Visuals :-

the whole course is divided in different parts contains different information of topics and respected it's exercises
are submitted here

Installation :-

Different parts will contain different installation like Node, React, Axious, JSON

Support :-

You can go to this link where you can find the whole course https://fullstackopen.com/

Project status :-

It is currently in learning stage where I am trying to learn how to use React, Redux, Node.js, MongoDB, GraphQL and TypeScript. exercises and work is push in this project on a daily bases