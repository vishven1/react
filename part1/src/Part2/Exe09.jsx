import { useState } from 'react'

const Exe09 = () => {
  const [persons, setPersons] = useState([{ name: 'Arto Hellas' , number: 7984563201}, { name: 'Ada Lovelace', number: '39-44-5323523'},
  { name: 'Dan Abramov', number: '12-43-234345'}]) 
  const [newName, setNewName] = useState('');
  const [newNum, setNum] = useState('');
  const [searchQuery, setSearchQuery] = useState('');

  const handleSearchChange = (event) => {
    setSearchQuery(event.target.value);
  };

  const filteredPersons = persons.filter(person =>
    person.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const handleNameChange = (event) => {
    setNewName(event.target.value)
  }

  const handleNumChange = (event) => {
    setNum(event.target.value)
  }
 
  const addPerson = (event) => {
    event.preventDefault()
    const newPerson = { name: newName , number: newNum}
    if (persons.some(person => person.name === newName) || persons.some(person => person.number === newNum)) {
        alert(`${newName} or ${newNum} is already added to the phonebook`)
      }
    else if(persons.some(person => person.name == "") || persons.some(person => person.number == "")){
        alert(`please enter a valid value`)
      }
    else{
        setPersons([...persons, newPerson])
        setNewName('')
        setNum('')
      } 
  }

  return (
    <div>
      <h2>Phonebook</h2>

      <div>
        Search: <input value={searchQuery} onChange={handleSearchChange} />
      </div>

      <h2> Add person </h2>
      <form onSubmit={addPerson}>
        <div>
          name: <input value={newName} onChange={handleNameChange}/>
        </div>
            <div>
                number: <input value={newNum} onChange={handleNumChange}/>
            </div>
        <div>
          <button type="submit">add</button>
        </div>
      </form>
      <h2>Numbers</h2>
      <div>
        {filteredPersons.map(person => (
          <div key={person.name}>{person.name} : {person.number}</div>
        ))}
      </div>
    
    </div>
  )
}

export default Exe09;