import Exe01 from "./Exe01"
import Exe02 from "./Exe02"
import Exe03 from "./Exe03"
import Exe04 from "./Exe04"
import Exe05 from "./Exe05"
import Exe06 from "./Exe06"
import Exe07 from "./Exe07"
import Exe08 from "./Exe08"
import Exe09 from "./Exe09"
import Exe10 from "./Exe10"
import Exe11 from "./Exe11"
import Exe12 from "./Exe12"
import Exe13 from "./Exe13"
import Exe14 from "./Exe14"
import Exe15 from "./Exe15"
import Exe16 from "./Exe16"
import Exe17 from "./Exe17"
import Exe18 from "./Exe18"
import Exe19 from "./Exe19"
import Exe20 from "./Exe20"
import Extra from "./Extra"
import FrontEnd from "./FrontEnd"
import ZStart from "./ZStart"

function AllPart2() {
    return (
      <>
      <div>   
        <ZStart/>
      </div>
      </>
    )
  }
  
export default AllPart2