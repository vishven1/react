import { useState } from 'react'

const SearchFilter = ({ value, onChange }) => {
    return (
      <div>
        Search: <input value={value} onChange={onChange} />
      </div>
    );
  };


const PersonForm = ({ newName, onNameChange, onSubmit, onNumChange, newNum }) => {
    return (
      <form onSubmit={onSubmit}>
        <div>
          Name: <input value={newName} onChange={onNameChange} />
          Number: <input value={newNum} onChange={onNumChange}/>
        </div>
        <div>
          <button type="submit">Add</button>
        </div>
      </form>
    );
};

const PersonList = ({ persons }) => {
    return (
      <div>
        <h2>Numbers</h2>
        <div>
          {persons.map(person => (
            <div key={person.name}>{person.name}: {person.number}</div>
          ))}
        </div>
      </div>
    );
  };


const Exe10 = () => {
  const [persons, setPersons] = useState([{ name: 'Arto Hellas' , number: 7984563201}, { name: 'Ada Lovelace', number: '39-44-5323523'},
  { name: 'Dan Abramov', number: '12-43-234345'},]) 
  const [newName, setNewName] = useState('');
  const [newNum, setNum] = useState('');
  const [searchQuery, setSearchQuery] = useState('');

  const handleSearchChange = (event) => {
    setSearchQuery(event.target.value);
  };

  const filteredPersons = persons.filter(person =>
    person.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const handleNameChange = (event) => {
    setNewName(event.target.value)
  }

  const handleNumChange = (event) => {
    setNum(event.target.value)
  }
 
  const addPerson = (event) => {
    event.preventDefault()
    const newPerson = { name: newName , number: newNum}
    if (persons.some(person => person.name === newName) || persons.some(person => person.number === newNum)) {
        alert(`${newName} or ${newNum} is already added to the phonebook`)
      }
    else if(persons.some(person => person.name == "") || persons.some(person => person.number == "")){
        alert(`please enter a valid value`)
      }
    else{
        setPersons([...persons, newPerson])
        setNewName('')
        setNum('')
      } 
  }

  return (
    <div>
      <h2>Phonebook</h2>
      <SearchFilter value={searchQuery} onChange={handleSearchChange} />
      <h3>Add a new person</h3>
      <PersonForm
        newName={newName}
        onNameChange={handleNameChange}
        onNumChange={handleNumChange}
        onSubmit={addPerson}
      />
      <PersonList persons={filteredPersons} />
    </div>
  )
}

export default Exe10;