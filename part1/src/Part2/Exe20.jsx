import React, { useState, useEffect } from 'react';
import axios from 'axios';
import.meta.env

const Exe20 = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [countries, setCountries] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [weather, setWeather] = useState(null);

  const CountryDetails = ({ country }) => {
    return (
      <div>
        <h2>{country.name.common}</h2>
        <p>Capital: {country.capital}</p>
        <p>Languages: {Object.values(country.languages).join(', ')}</p>
        <p>Region: {country.region}</p>
        <img src={country.flags.svg} alt="Flag" style={{ width: '150px', height: 'auto' }} />
        {weather && (
          <div>
            <h3>Weather in {country.capital[0]}</h3>
            <p>Description: {weather.weather[0].description}</p>
            <p>Temperature: {(weather.main.temp - 273.15).toFixed(2)}°C</p>
            <img src={`http://openweathermap.org/img/wn/${weather.weather[0].icon}.png`} alt="Weather Icon" />
            <p>Humidity: {weather.main.humidity}%</p>

          </div>
        )}
        
      </div>
    );
  };

  const CountryList = ({ country }) => {
    const handleViewDetails = () => {
      setSelectedCountry(country);
    };
    return (
      <div>
        <span>{country.name.common} </span> 
        <button onClick={handleViewDetails}> View</button>
      </div>
    );
  };

  useEffect(() => {
    if (searchQuery) {
      axios.get(`https://restcountries.com/v3.1/name/${searchQuery}`)
        .then(response => {
          setCountries(response.data);
        })
        .catch(error => {
          console.error('Error fetching countries:', error);
        });
    }
  }, [searchQuery]);

  useEffect(() => {
    if (selectedCountry) {
      axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${selectedCountry.capital}&appid=bfd9eaf9b7d899536bf7f026c90caf37`)
    
        .then(response => {
          setWeather(response.data);
        })
        .catch(error => {
          console.error('Error fetching weather data:', error);
        });
    }
  }, [selectedCountry]);

  const handleInputChange = (event) => {
    setSearchQuery(event.target.value);
  };

  return (
    <div>
      <h1> Find Countries </h1>
      <input type="text" placeholder="Search for a country..." value={searchQuery} onChange={handleInputChange}
      />
      {selectedCountry ? (
        <CountryDetails country={selectedCountry} />
      ) : (
        countries.length > 10 ? (
          <p>Too many matches, please specify your search.</p>
        ) : (
          <div>
            {countries.map(country => (
              <CountryList key={country.name.common} country={country} />
            ))}
          </div>
        )
      )}
    </div>
  );
};

export default Exe20;
