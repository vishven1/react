import React from 'react'

export default function Extra() {
  return (
    <div>
      
    </div>
  )
}


// import React, { useState, useEffect } from 'react';
// import axios from 'axios';

// const Extra = () => {
//   const [searchQuery, setSearchQuery] = useState('');
//   const [countries, setCountries] = useState([]);
//   const [selectedCountry, setSelectedCountry] = useState(null);
//   const [weatherData, setWeatherData] = useState(null);
//   const [loadingWeather, setLoadingWeather] = useState(false);
//   const [weatherError, setWeatherError] = useState(null);

//   useEffect(() => {
//     if (searchQuery) {
//       axios.get(`https://restcountries.com/v3.1/name/${searchQuery}`)
//         .then(response => {
//           setCountries(response.data);
//         })
//         .catch(error => {
//           console.error('Error fetching countries:', error);
//         });
//     }
//   }, [searchQuery]);

//   useEffect(() => {
//     if (selectedCountry && selectedCountry.capital) {
//       setLoadingWeather(true);
//       axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${selectedCountry.capital}&appid=54l41n3n4v41m34rv0`)
//         .then(response => {
//           setWeatherData(response.data);
//           setLoadingWeather(false);
//           setWeatherError(null);
//         })
//         .catch(error => {
//           console.error('Error fetching weather:', error);
//           setLoadingWeather(false);
//           setWeatherError('Failed to fetch weather data');
//         });
//     }
//   }, [selectedCountry]);

//   const handleInputChange = (event) => {
//     setSearchQuery(event.target.value);
//   };

//   const handleViewDetails = (country) => {
//     setSelectedCountry(country);
//   };

//   const WeatherInfo = () => {
//     if (loadingWeather) {
//       return <p>Loading weather...</p>;
//     }
//     if (weatherError) {
//       return <p>{weatherError}</p>;
//     }
//     if (weatherData) {
//       return (
//         <div>
//           <h3>Weather in {selectedCountry.capital}</h3>
//           <p>Temperature: {weatherData.main.temp} Kelvin</p>
//           <p>Description: {weatherData.weather[0].description}</p>
//           {weatherData.weather[0].icon && (
//             <img src={`http://openweathermap.org/img/wn/${weatherData.weather[0].icon}.png`} alt="Weather Icon" />
//           )}
//         </div>
//       );
//     }
//     return null;
//   };

//   return (
//     <div>
//       <h1>Country Information</h1>
//       <input
//         type="text"
//         placeholder="Search for a country..."
//         value={searchQuery}
//         onChange={handleInputChange}
//       />
//       {selectedCountry ? (
//         <div>
//           <h2>{selectedCountry.name.common}</h2>
//           <p>Capital: {selectedCountry.capital}</p>
//           <p>Population: {selectedCountry.population}</p>
//           <p>Region: {selectedCountry.region}</p>
//           <WeatherInfo />
//         </div>
//       ) : (
//         countries.length > 10 ? (
//           <p>Too many matches, please specify your search.</p>
//         ) : (
//           <div>
//             {countries.map(country => (
//               <div key={country.name.common}>
//                 <span>{country.name.common}</span>
//                 <button onClick={() => handleViewDetails(country)}>View</button>
//               </div>
//             ))}
//           </div>
//         )
//       )}
//     </div>
//   );
// };

// export default Extra;
