import { useEffect, useState } from 'react'
import axios from 'axios';

const Exe16 = () => {
  const [persons, setPersons] = useState([]) 
  const [newName, setNewName] = useState('');
  const [newNum, setNum] = useState('');
  const [searchQuery, setSearchQuery] = useState('');
  const [notification, setNotification] = useState('');

  useEffect(() => {
    getAllPersons()
      .then((data) => {
        setPersons(data);
      })
  }, [persons]);

  const showNotification = (message) => {
    setNotification(message);
    setTimeout(() => {
      setNotification(null);
    }, 3000);
  };

  const handleSearchChange = (event) => {
    setSearchQuery(event.target.value);
  };

  const filteredPersons = persons.filter(person =>
    person.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const handleNameChange = (event) => {
    setNewName(event.target.value)
  }

  const handleNumChange = (event) => {
    setNum(event.target.value)
  }

  const getAllPersons = () => {
    return axios.get('http://localhost:3001/persons')
    .then((response) => response.data)
      .catch((error) => {
        console.error('Error fetching persons:', error);
        throw error;
      });
  };

  const addPerson = (event) => {
    event.preventDefault()
    const oldPerson = persons.find(person =>  person.name.toLowerCase() === newName.toLowerCase());
    const newPerson = { name: newName , number: newNum}
    if (oldPerson) {
        if(window.confirm(` Do you want to replace number of ${newName}`)){
            axios.put(`http://localhost:3001/persons/${oldPerson.id}`, newPerson)
              .then((res)=> {
                console.log(res)
                setPersons(persons.map(person => person.name !== newName? person : newPerson));
                setNewName('');
                setNum('');
              })
        }

      }
    else if(persons.some(person => person.name == "") || persons.some(person => person.number == "")){
        alert(`please enter a valid value`)
      }
    else{
      axios.post("http://localhost:3001/persons", newPerson)
       .then(response => {
         console.log(response)
         setPersons([...persons, newPerson]);
         setNotification("Data Added")
         showNotification(`Successfully added ${newPerson.name}`);
         setNewName('')
         setNum('')
         })
      }     
  }

  const eleDelete =(id)=>{
    const personToDelete = persons.find(person => person.id === id);
    if(window.confirm(`Are you sure you want to delete "${personToDelete.name}" from record`))
        axios.delete(`http://localhost:3001/persons/${id}`)
          .then(() => {
            setPersons(persons.filter(person => person.id !== id))
            console.log("deleted")
          }).catch((err)=>{`error `, err})
  }

  return (
    <div>
      <center>
      <h2>Phonebook</h2>
      {notification && <div className="notification">{notification}</div>}
      <div>
        Search By Name : <input value={searchQuery} onChange={handleSearchChange} />
      </div>

      <h2> Add person </h2>
      <form onSubmit={addPerson}>
        <div>
          Enter Name: <input value={newName} onChange={handleNameChange}/>
        </div>
            <div>
               Enter Number: <input value={newNum} onChange={handleNumChange}/>
            </div>
        <div> <br />
          <button type="submit"> Add Data </button>
        </div>
      </form>
      <h2>Numbers</h2>
      <div>
        {filteredPersons.map(person => (
          <div key={person.name}>{person.name} : {person.number} {"  - "}
          <button onClick={()=>eleDelete(person.id)}>   Delete </button>
          </div>
        ))}
      </div>   
      </center> 
    </div>
  )
}

export default Exe16;