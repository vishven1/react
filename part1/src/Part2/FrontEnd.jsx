import { useEffect, useState } from 'react'
import axios from 'axios';

//final with validations
//this contains Exercise from 3.19 to 3.21

const FrontEnd = () => {
  const [persons, setPersons] = useState([]) 
  const [newName, setNewName] = useState('');
  const [newNum, setNum] = useState('');
  const [searchQuery, setSearchQuery] = useState('');
  const [notification, setNotification] = useState('');
  const [err, setErr] = useState('');

  useEffect(() => {
    getAllPersons()
      .then((data) => {
        setPersons(data);
      })
  }, [persons]);

  const showNotification = (message) => {
    setNotification(message);
    setTimeout(() => {
      setNotification(null);
    }, 3000);
  };

  const alertmsg = (message) => {
    setErr(message);
    setTimeout(() => {
      setErr(null)
    }, 3000);
  }

  const handleSearchChange = (event) => {
    setSearchQuery(event.target.value);
  };

  const filteredPersons = persons.filter(person =>
    person.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const handleNameChange = (event) => {
    setNewName(event.target.value)
  }

  const handleNumChange = (event) => {
    setNum(event.target.value)
  }

  const getAllPersons = () => {
    return axios.get('http://localhost:3001/api/persons')
    .then((response) => response.data)
      .catch((error) => {
        console.error('Error fetching persons:', error);
        throw error;
      });
  };

  const addPerson = (event) => {
    event.preventDefault()
    const isValidNumber = /^\d+$/.test(newNum);
    const isValidName = /^[A-Za-z\s]+$/.test(newName);
    const oldPerson = persons.find(person =>  person.name.toLowerCase() === newName.toLowerCase());
    const newPerson = { name: newName , number: newNum}
    if (newName.length < 3) {
      alertmsg('Name must be more then 2 letters', false);
      return;
    }else if(newNum.length !== 10){
      alertmsg('Number  must be of 10 digits', false);
      return;
    }
    else if(!isValidNumber){
      alertmsg('Number must contain digits only', false);
      return;
    }
    else if(!isValidName){
      alertmsg('Name must contain letters only', false);
      return;
    }
    else if(newName==="" || newNum===""){
      alertmsg("Please Enter a valid Name or Number", false)
      return;
    }
    if (oldPerson) {
        if(window.confirm(` Do you want to replace number of ${newName}`)){
            axios.put(`http://localhost:3001/api/persons/${oldPerson.id}`, newPerson)
              .then((res)=> {
                console.log(res)
                setPersons(persons.map(person => person.name !== newName? person : newPerson))
                showNotification(`Successfully replaced ${newPerson.name}`);
                setNewName('')
                setNum('').catch(error => {
                  console.error('Error updating number:', error);
                  showNotification('Failed to update number', false);
                });
              })
        }
      }
    else{
      axios.post("http://localhost:3001/api/persons", newPerson)
       .then(response => {
         console.log(response)
         setPersons([...persons, newPerson]);
         setNotification("Data Added")
         showNotification(`Successfully added ${newPerson.name}`);
         setNewName('')
         setNum('').catch(error => {
          console.error('Error adding person:', error);
          showNotification('Failed to add person', false);
        })
         })
      }     
  }

  const eleDelete =(id)=>{
    const personToDelete = persons.find(person => person.id === id);
    if(window.confirm(`Are you sure you want to delete "${personToDelete.name}" from record`))
        axios.delete(`http://localhost:3001/api/persons/${id}`)
          .then(() => {
            setPersons(persons.filter(person => person.id !== id))
            showNotification(`Successfully Deleted ${personToDelete.name}`);
            console.log("deleted")
          }).catch((err)=>{`error `, err})
  }

  return (
    <div className="full">
      <center>
      <h2>Phonebook</h2>
      {notification && <div className="notification">{notification}</div>}
      {err && <div className="redNotification">{err}</div>}
      <div>
        Search By Name : <input value={searchQuery} onChange={handleSearchChange} />
      </div>

      <h2> Add person </h2>
      <form onSubmit={addPerson}>
        <div>
          Enter Name: <input value={newName} onChange={handleNameChange}/>
        </div>
            <div>
               Enter Number: <input value={newNum} onChange={handleNumChange}/>
            </div>
        <div> <br />
          <button type="submit"> Add Data </button>
        </div>
      </form>
      <h2>Numbers</h2>
      <div>
        {filteredPersons.map(person => (
          <div key={person.name}>{person.name} : {person.number} {"  - "}
          <button onClick={()=>eleDelete(person.id)}>   Delete </button>
          </div>
        ))}
      </div>   
      </center> 
    </div>
  )
}

export default FrontEnd;