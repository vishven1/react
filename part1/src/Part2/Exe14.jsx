import { useEffect, useState } from 'react'
import axios from 'axios';

const Exe14 = () => {
  const [persons, setPersons] = useState([]) 
  const [newName, setNewName] = useState('');
  const [newNum, setNum] = useState('');
  const [searchQuery, setSearchQuery] = useState('');

  useEffect(() => {
    getAllPersons()
      .then((data) => {
        setPersons(data);
      })
  }, []);

  const handleSearchChange = (event) => {
    setSearchQuery(event.target.value);
  };

  const filteredPersons = persons.filter(person =>
    person.name.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const handleNameChange = (event) => {
    setNewName(event.target.value)
  }

  const handleNumChange = (event) => {
    setNum(event.target.value)
  }

  const getAllPersons = () => {
    return axios.get('http://localhost:3001/api/persons')
    .then((response) => response.data)
      .catch((error) => {
        console.error('Error fetching persons:', error);
        throw error;
      });
  };

  const addPerson = (event) => {
    event.preventDefault()
    const newPerson = { name: newName , number: newNum}
    if (persons.some(person => person.name === newName) || persons.some(person => person.number === newNum)) {
        alert(`${newName} or ${newNum} is already added to the phonebook`)
      }
    else if(persons.some(person => person.name == "") || persons.some(person => person.number == "")){
        alert(`please enter a valid value`)
      }
    else{
      axios.post("http://localhost:3001/api/persons", newPerson)
       .then(response => {
         console.log(response)
         setPersons([...persons, newPerson]);
         setNewName('')
         setNum('')
         alert(`${newName} added successfully `)} ).catch(error => {
           alert(`unable to add ${newName}, try again later`)
         })
      }     
  }

  const eleDelete =(id)=>{
    const personToDelete = persons.find(person => person.id === id);
    if(window.confirm(`do you want to delete ${personToDelete.name}`))
        axios.delete(`http://localhost:3001/api/persons/${id}`)
          .then(() => {
            setPersons(persons.filter(person => person.id !== id))
            console.log("deleted")
          }).catch((err)=>{`error `, err})
  }

  return (
    <div>
      <h2>Phonebook</h2>

      <div>
        Search: <input value={searchQuery} onChange={handleSearchChange} />
      </div>

      <h2> Add person </h2>
      <form onSubmit={addPerson}>
        <div>
          name: <input value={newName} onChange={handleNameChange}/>
        </div>
            <div>
                number: <input value={newNum} onChange={handleNumChange}/>
            </div>
        <div>
          <button type="submit">add</button>
        </div>
      </form>
      <h2>Numbers</h2>
      <div>
        {filteredPersons.map(person => (
          <div key={person.name}>{person.name} : {person.number} {"  - "}
          <button onClick={()=>eleDelete(person.id)}>   delete </button>
          </div>
          
        ))}
      </div>    
    </div>
  )
}

export default Exe14