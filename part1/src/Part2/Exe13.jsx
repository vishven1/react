import { useEffect, useState } from 'react'
import axios from 'axios';

const getAllPersons = () => {
    return axios.get('http://localhost:3001/persons')
      .then((response) => response.data)
      .catch((error) => {
        console.error('Error fetching persons:', error);
        throw error; 
      });
  };

const addNewPerson = (newPerson) => {
    return axios.post('http://localhost:3001/persons', newPerson)
      .then((response) => response.data)
      .catch((error) => {
        console.error('Error adding person:', error);
        throw error; 
      });
  };

const Exe13 = () => {
  const [persons, setPersons] = useState([]) 
  const [newName, setNewName] = useState('');
  const [newNum, setNum] = useState('');
  const [searchQuery, setSearchQuery] = useState('');

  useEffect(() => {
    getAllPersons()
      .then((data) => {
        setPersons(data);
      })
  }, []);


  const handleSearchChange = (event) => {
    setSearchQuery(event.target.value);
  };

  const filteredPersons = persons.filter(person =>
    person.name.toLowerCase().includes(searchQuery.toLowerCase())
  ); 

  const handleNameChange = (event) => {
    setNewName(event.target.value)
  }

  const handleNumChange = (event) => {
    setNum(event.target.value)
  }
 
  const addPerson = (event) => {
    event.preventDefault();
    const newPerson = { name: newName, number: newNum };
    addNewPerson(newPerson)
      .then((newPerson) => {
        setPersons([...persons, newPerson]);
        setNewName('');
        setNum('');
      })
      .catch((error) => {
        alert('Failed to add person. Please try again.',error);
      });
  };
    
  return (
    <div>
      <h2>Phonebook</h2>

      <div>
        Search: <input value={searchQuery} onChange={handleSearchChange} />
      </div>

      <h2> Add person </h2>
      <form onSubmit={addPerson}>
        <div>
          name: <input value={newName} onChange={handleNameChange}/>
        </div>
            <div>
                number: <input value={newNum} onChange={handleNumChange}/>
            </div>
        <div>
          <button type="submit">add</button>
        </div>
      </form>
      <h2>Numbers</h2>
      <div>
        {filteredPersons.map(person => (
          <div key={person.name}>{person.name} : {person.number}</div>
        ))}
      </div>
    </div>
  )
}

export default Exe13