import { useState } from 'react'

const Exe07 = () => {
  const [persons, setPersons] = useState([{ name: 'Arto Hellas' }]) 
  const [newName, setNewName] = useState('')

  const handleNameChange = (event) => {
    setNewName(event.target.value)
  }
 
  const addPerson = (event) => {
    event.preventDefault()
    const newPerson = { name: newName }
    if (persons.some(person => person.name === newName)) {
        alert(`${newName} is already added to the phonebook`)
      }else{
        setPersons([...persons, newPerson])
        setNewName('')
      } 
  }

  return (
    <div>
      <h2>Phonebook</h2>
      <form onSubmit={addPerson}>
        <div>
          name: <input value={newName} onChange={handleNameChange}/>
        </div>
        <div>
          <button type="submit">add</button>
        </div>
      </form>
      <h2>Numbers</h2>
      <div>
        {persons.map(person => (
          <div key={person.name}>{person.name}</div>
        ))}
      </div>
    
    </div>
  )
}

export default Exe07;