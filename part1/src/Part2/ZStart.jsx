// import React, { useState } from 'react';
// import FrontEnd from "./FrontEnd"

// const ZStart = () => {
//   const [password, setPassword] = useState('');
//   const [errorMessage, setErrorMessage] = useState('');

//   const correctPassword = 'Stegowl';

//   const handleSubmit = (event) => {
//     event.preventDefault();
    
//     if (password === correctPassword) {
//       // Redirect to the main page or grant access
//       // Example: window.location.href = '/main-page';
//       window.location.href = 'src/Part2/FrontEnd';
//       console.log('Access granted');
//     } else {
//       setErrorMessage('Incorrect password. Please try again.');
//     }
//   };
// //   F:\Stegowl\react\part1\src\Part2\FrontEnd.jsx
//   return (
//     <>
//     <div>
//       <h2>Login Page</h2>
//       <form onSubmit={handleSubmit}>
//         <div>
//           <label htmlFor="passwordInput">Password:</label>
//           <input type="password" id="passwordInput" value={password} onChange={(e) => setPassword(e.target.value)} />
//         </div>
//             {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}
//         <div>
//           <button type="submit">Login</button>
//         </div>
//       </form>
//     {/* <form>
//             <div className="form-group">
//                 <label for="exampleInputEmail1">Email address</label>
//                 <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/>
//                 <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
//             </div>
//             <div className="form-group">
//                 <label for="exampleInputPassword1">Password</label>
//                 <input type="password"  id="passwordInput" value={password} onChange={(e) => setPassword(e.target.value)} className="form-control" placeholder="Password" />
//             </div>
//             <div className="form-group form-check">
//                 <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
//                 <label className="form-check-label" for="exampleCheck1">Check me out</label>
//             </div>
//             <button type="submit" className="btn btn-primary">Submit</button>
//     </form>  */}
//     </div>  
//     </>
//   );
// };

// export default ZStart;


import React, { useState } from 'react';
import FrontEnd from './FrontEnd';
import './Style.css';

const ZStart = () => {
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const correctEmail = 'vishven.stegowl@gmail.com';
  const correctPassword = 'Vishven@123';
  const [loggedIn, setLoggedIn] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (email === correctEmail && password === correctPassword) {
      setLoggedIn(true);
    } else {
      setErrorMessage('Incorrect email or password. Please try again.');
    }
  };

  return (
    <div className='back'>
    <div className="container"> 
      {!loggedIn ? (
        <div>
            <h3> PhoneBook Data </h3>
          <h2> Login Page</h2>
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <label htmlFor="email">Email:</label>
              <input type="email" id="emailInput" value={email} onChange={(e) => setEmail(e.target.value)} />
            </div>
            <div className="form-group">
              <label htmlFor="passwordInput">Password:</label>
              <input type="password" id="passwordInput" value={password} onChange={(e) => setPassword(e.target.value)} />
            </div>
            {errorMessage && <p className="error-message">{errorMessage}</p>}
            <div>
              <button type="submit">Login</button>
            </div>
          </form>
        </div>
      ) : (
        <FrontEnd />
      )}
    </div>
    </div>
  );
};

export default ZStart;





