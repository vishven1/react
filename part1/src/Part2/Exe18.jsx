import  axios  from 'axios'
import React, { useEffect, useState } from 'react'

export default function Exe18() {
  const [searchQuery, setSearchQuery] = useState('');
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    if (searchQuery) {
      axios.get(`https://restcountries.com/v3.1/name/${searchQuery}`)
        .then(response => {
          setCountries(response.data);
        })
        .catch(error => {
          console.error('Error fetching countries:', error);
        });
    }
  }, [searchQuery]);

  const handleNameChange = (event) => {
    setSearchQuery(event.target.value)
      }

  const displayResult = () => {
    if (countries.length === 0) {
      return <p>No countries found.</p>;
    } else if (countries.length > 10) {
      return <p>Too many matches, please specify your search.</p>;
    } else if (countries.length === 1) {
      const country = countries[0];
      return (
        <div>
          <h2>{country.name.common}</h2>
          <p>Capital: {country.capital}</p>
          <p>Population: {country.population}</p>
          <p>Region: {country.region}</p>
        </div>
      );
    } else {
      return (
        <ul>
          {countries.map(country => (
            <li key={country.name.common}>{country.name.common}</li>
          ))}
        </ul>
      );
    }
  };    

  return (
    <div>
     Find Countries 
      <form>
      <input type="text" placeholder="Search for a country..." value={searchQuery} onChange={handleNameChange}/>
      {displayResult()} 
      </form>
    </div>
  )
}
