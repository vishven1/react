const Course = ({ course }) => {
    const total = course.parts.reduce((sum, part) => sum + part.exercises, 0);
    return (
      <div>
        <h2>{course.name}</h2>
        <div>
          {course.parts.map(part => (
            <div key={part.id}>
              <p>{part.name}</p>
              <p>{part.exercises}</p>
            </div>
          ))}
          <p>Total Exercises: {total}</p>
        </div>
      </div>
    );
  };

export default Course;