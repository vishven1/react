import React from 'react'


const Course = ({ course }) => {
    const total = course.parts.reduce((sum, part) => sum + part.exercises, 0);
    return (
      <div>
        <h2>{course.name}</h2>
        <div>
          {course.parts.map(part => (
            <div key={part.id}>
              <p>{part.name}</p>
              {part.exercises}   
            </div>
          ))}
           <p>Total Exercises: {total}</p>
        </div>
      </div>
    );
  };

export default function Exe03() {
    const course = {
        id: 1,
        name: 'Half Stack application development',
        parts: [
          {
            name: 'Fundamentals of React',
            exercises: 10,
            id: 1
          },
          {
            name: 'Using props to pass data',
            exercises: 7,
            id: 2
          },
          {
            name: 'State of a component',
            exercises: 14,
            id: 3
          }
        ]
      }

  return (
    <div>
      <Course course={course}/>
    </div>
  )
}