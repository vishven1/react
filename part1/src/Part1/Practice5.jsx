import React from 'react'
import Header from './Header';

const Headerr = ({ course }) => {
    return (
      <h1>{course}</h1>
    );
  };

const Content = ({ parts }) => {
  return (
    <div> 
      {parts.map(part => (
        <p key={part.name}>
          {part.name} {part.exercises}
        </p>
      ))}
    </div>
  );
};

const Total = ({ parts }) => {
    let totalExercises = 0;
    for (let i = 0; i < parts.length; i++) {
        totalExercises += parts[i].exercises;
        console.log(totalExercises);
      }
    return (
     <p>Total number of exercises: {totalExercises}</p>
    );
  };


export default function Practice5() {
const course = {
    name: 'Half Stack application development',
    parts: [
      {
        name: 'Fundamentals of React',
        exercises: 10
      },
      {
        name: 'Using props to pass data',
        exercises: 7
      },
      {
        name: 'State of a component',
        exercises: 14
      }
    ]
  };

  return (
    <div>
      <Header num="5" title="course information step 5"/>   
      <Headerr course={course.name} />
      <Content parts={course.parts} />
      <Total parts={course.parts} />
    </div>
  );
}
