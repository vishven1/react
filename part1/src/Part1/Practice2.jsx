import React from 'react';
import Header from './Header';

const Part = ({ name, exercises }) => {
  return <p>{name} {exercises}</p>;
};

const Practice2 = () => {
  const part1 = 'Fundamentals of React';
  const exercises1 = 10;
  const part2 = 'Using props to pass data';
  const exercises2 = 7;
  const part3 = 'State of a component';
  const exercises3 = 14;

  return (
    <>
    <Header num="2" title="course information"/>
    <div>
      <Part name={part1} exercises={exercises1} />
      <Part name={part2} exercises={exercises2} />
      <Part name={part3} exercises={exercises3} />
    </div>
    </>
  );
};

export default Practice2;
