import React from 'react';
import Header from './Header';

const Headerr = ({ course }) => {
    return (
      <h1>{course}</h1>
    );
  };

const Content = ({ parts }) => {
  return (
    <div>
      {parts.map(part => (
        <p key={part.name}>
          {part.name} {part.exercises}
        </p>
      ))}
    </div>
  );
};

const Total = ({ parts }) => {
    let totalExercises = 0;
    for (let i = 0; i < parts.length; i++) {
        totalExercises += parts[i].exercises;
        console.log(totalExercises);
      }
    return (
     <p>Total number of exercises: {totalExercises}</p>
    );
  };

const Practice4 = () => {    
    const course = 'Half Stack application development';
    const parts = [
      {
        name: 'Fundamentals of React',
        exercises: 10
      },
      {
        name: 'Using props to pass data',
        exercises: 7
      },
      {
        name: 'State of a component',
        exercises: 14
      }
    ];
  
    return (
    <>
    <Header num="4" title="course information"/>
    <div>
        <Headerr course={course} />
        <Content parts={parts} />
        <Total parts={parts} />
    </div>
    </>
    );
  };

export default Practice4;
