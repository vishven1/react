import React from 'react';
import Header from './Header';

export default function Practice3(){
  const course = 'Half Stack application development';
  const obj1 = {
    name: 'Fundamentals of React',
    exercises: 10
  };
  const address = 'name';
  console.log(obj1[address]);
  
  const obj2 = {
    name: 'Using props to pass data',
    exercises: 7
  };
  const obj3 = {
    name: 'State of a component',
    exercises: 14
  };
  
  return (
    <>
    <Header num="3" title="course information"/>
    <div>
      <h1>{course}</h1>
      <p>
        {obj1.name} {obj1.exercises} {obj1.address}
      </p>
      <p>
        {obj2.name} {obj2.exercises}
      </p>
      <p>
        {obj3.name} {obj3.exercises}
      </p>
    </div>
    </>
  );
};
