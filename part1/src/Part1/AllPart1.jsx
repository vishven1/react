import React from 'react'
import Practice2 from './Practice2'
import Practice3 from './Practice3'
import Header from './Header'
import Footer from './Footer'
import Practice4 from './Practice4'
import Practice5 from './Practice5'
import Extra from './Extra'
import Practice6 from './Practice6'
import Practice7 from './Practice7'
import Practice8 from './Practice8'
import Practice9 from './Practice9'
import Practice10 from './Practice10'
import Practice11 from './Practice11'
import Practice12 from './Practice12'
import Practice13 from './Practice13'
import Practice14 from './Practice14'

function AllPart1() {
  return (
    <>
    <div>   
      <Practice14 title="Practice 9"/>
    </div>
    </>
  )
}

export default AllPart1
