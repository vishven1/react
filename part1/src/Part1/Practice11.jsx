import { useState } from "react"
import Header from "./Header"

const Button = ({ handleClick, text }) => {
    return <button onClick={handleClick}>{text}</button>;
  };

const StatisticLine = ({ text, value }) => {
    return (
      <p>
        {text}: {value}
      </p>
    );
  };

const Statistics = ({ good, neutral, bad }) => {
    const totalFeedback = good + neutral + bad;
    const avegScore = ((good+neutral+bad)/3) || 0;
    const Percentage = (good / totalFeedback) * 100 || 0;

  return (
    <div>
        <h2>Statistics</h2>
        {totalFeedback === 0 ? ("No feedback given "): 
        (<div>
        <table>
        <ul>
        <tbody>
        <StatisticLine text="Good" value={good}/> 
        <StatisticLine text="Neutral" value={neutral}/>
        <StatisticLine text="Bad" value={bad}/>
        <StatisticLine text="Total Feedback" value={totalFeedback}/>
        <StatisticLine text="Average Score" value={avegScore}/>
        <StatisticLine text="Percentage " value={Percentage}/>
        </tbody>
        </ul>
        </table>
        </div>)}
    </div>
  );
  }
  
const Practice11 = () => {
    const [good, setGood] = useState(0)
    const [neutral, setNeutral] = useState(0)
    const [bad, setBad] = useState(0)

    const GoodClick=()=>{
        setGood(good+1);
    }
    const NeutralClick=()=>{
        setNeutral(neutral+1);
    }
    const BadClick=()=>{
        setBad(bad+1);
    }

    return (
        <div>
        {/* <center> */}
        <Header num="11" title="unicafe "/>
        <h1> Give your feedback </h1>
        <Button handleClick={GoodClick} text="Good" />
        <Button handleClick={NeutralClick} text="Neutral" />
        <Button handleClick={BadClick} text="Bad" />
      
        <Statistics good={good} neutral={neutral} bad={bad} />      
        {/* </center> */}
        </div>
    ) 
  }

export default Practice11;