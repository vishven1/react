import { useState } from "react"
import Header from "./Header"

const Statistics = ({ good, neutral, bad }) => {

    const totalFeedback = good + neutral + bad;
    const avegScore = ((good+neutral+bad)/3) || 0;
    const Percentage = (good / totalFeedback) * 100 || 0;

  return (
    <div>
        <h2>Statistics</h2>
        good :{good} <br />
        neutral :{neutral} <br />
        bad :{bad} <br />
        <p>Total feedback: {totalFeedback}</p>
        <p>Average score: {avegScore}</p>
        <p>Percentage of positive feedback: {Percentage}%</p>
    </div>
  );
  }
  
const Practice8 = () => {
    const [good, setGood] = useState(0)
    const [neutral, setNeutral] = useState(0)
    const [bad, setBad] = useState(0)

    return (
        <div>
        <center>
        <Header num="8" title="unicafe "/>
        <h1> Give your feedback </h1>
        <button onClick={()=>setGood(good+1)}>Good</button>
        <button onClick={()=>setNeutral(neutral+1)}>Neutral</button>
        <button onClick={()=>setBad(bad+1)}>Bad</button>

        
        <Statistics good={good} neutral={neutral} bad={bad} />
        
        </center>
        </div>
    )
  }

export default Practice8;