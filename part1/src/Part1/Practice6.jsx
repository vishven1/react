import React, { useState } from 'react'
import Header from './Header'

export default function Practice6() {
    const [good, setGood] = useState(0)
    const [neutral, setNeutral] = useState(0)
    const [bad, setBad] = useState(0)

    
    return (
        <div>
        <center>
        <Header num="6" title="unicafe "/>
        <h1> Give your feedback </h1>
        <button onClick={()=>setGood(good+1)}>Good</button>
        <button onClick={()=>setNeutral(neutral+1)}>Neutral</button>
        <button onClick={()=>setBad(bad+1)}>Bad</button>

        <h2>Statistics</h2>
        good {good} <br />
        neutral {neutral} <br />
        bad {bad} <br />
        </center>
        </div>
        
    )
}
