import React from 'react'

export default function Header(props) {
  return (
    <div>
      <center> 
      <h1>Exercise {props.num} </h1> 
      <h3>{props.title}</h3>
      </center>
    </div>
  )
}
