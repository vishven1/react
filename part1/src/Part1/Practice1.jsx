import React from 'react'
import Header from './Header';
import Footer from './Footer';

function Pratice1(props) {
  const course = 'Half Stack application development';
  const part1 = 'Fundamentals of React';
  const exercises1 = 10;
  const part2 = 'Using props to pass data';
  const exercises2 = 7;
  const part3 = 'State of a component';
  const exercises3 = 14;

  const Headerr = ({ course }) => {
    return <h2>{course}</h2>;
  };

  const Content = ({ part1, exercises1, part2, exercises2, part3, exercises3 }) => {
    return (    
      <div>
        <p>Part1 : {part1}  Exercise: {exercises1}</p>
        <p>Part2 : {part2} Exercise : {exercises2}</p>
        <p>Part3 : {part3} Exercise : {exercises3}</p>
      </div>
    );
  };

  const Total = ({ exercises1, exercises2, exercises3 }) => {
    const totalExercises = exercises1 + exercises2 + exercises3;
    return <p>Number of exercises {totalExercises}</p>;
  };

  console.log('Hello from component')
  return (
    <>
    <Header num="1" title="course information"/>
     <Headerr course={course} />
      <Content
        part1={part1}
        exercises1={exercises1}
        part2={part2}
        exercises2={exercises2}
        part3={part3}
        exercises3={exercises3}
      />
      <Total
        exercises1={exercises1}
        exercises2={exercises2}
        exercises3={exercises3}
      />  
    <Footer/>
    </>
  )
}

export default Pratice1
